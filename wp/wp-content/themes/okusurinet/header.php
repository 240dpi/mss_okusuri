<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="imagetoolbar" content="no" /> <!-- IEのイメージツールバー 無効 -->
<link rel="profile" href="//gmpg.org/xfn/11">
<!-- <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> -->
<!-- <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" /> -->
<meta name="apple-mobile-web-app-title" content="お薬NET">

<?php
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', array());
?>

<?php wp_head(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/glide.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo("stylesheet_url"); ?>?v=<?php echo filemtime(get_stylesheet_directory() . '/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/glide.core.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/glide.theme.css">

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/smooth-scroll.js"></script>

</head>

<body <?php body_class(); ?>>
	
<!-- // container -->
<div id="container" class="">
	<header>
		<?php if(is_home()): ?>
		<?php else: ?>
			<?php include(TEMPLATEPATH . '/temp/temp_g_nav.php'); ?>
		<?php endif; ?>
	</header>
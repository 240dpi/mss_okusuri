$(function() {
	/* ユーザーエージェント iOS */
	var ua = {};
	ua.name = window.navigator.userAgent.toLowerCase();
	 
	ua.isIE = (ua.name.indexOf('msie') >= 0 || ua.name.indexOf('trident') >= 0);
	ua.isiPhone = ua.name.indexOf('iphone') >= 0;
	ua.isiPod = ua.name.indexOf('ipod') >= 0;
	ua.isiPad = ua.name.indexOf('ipad') >= 0;
	ua.isiOS = (ua.isiPhone || ua.isiPod || ua.isiPad);
	ua.isAndroid = ua.name.indexOf('android') >= 0;
	ua.isTablet = (ua.isiPad || (ua.isAndroid && ua.name.indexOf('mobile') < 0));
	 
	if (ua.isiOS) {
	    $('body').addClass('i-os');
	}
	/* ユーザーエージェント iOS END */
	
	// ナビ固定
	var fixnav = function() {
		var classname = 'h-nav_fix';
		var $nav = $('.h-nav');
		var $outer = $('.menu-bar');
		var $spacer = $('.spacer-nav');
		$(window).scroll(function() {
			var top = $(window).scrollTop();
			if (top > $outer.offset().top + 180) {
				$nav.addClass(classname);
				$spacer.addClass('spacer-nav_fix');
				if ($nav.css('top') != '0px') {
					$nav.css({
						top: 0
					});
				}
			} else if ($nav.hasClass(classname) && top < $outer.offset().top + $nav.height()) {
				$nav.css({
					top: -80 //-$outer.height()
				});
				$nav.removeClass(classname);
				//$nav.removeClass('h-nav_fixnod');
				$spacer.removeClass('spacer-nav_fix');
			}
		});
	}
	fixnav();

});

// スムーススクロールを起動する
smoothScroll.init({
		speed: 1000 , // 目的地に到達するまでの時間を1秒(1000ミリ秒)に変更
		updateURL: false, // URLを[#〜]に変更したくない
		easing: 'easeInOutQuart', // スピードの種類
		offset: 48, // 到達場所からズラすピクセル数
		callback: function () {} // コールバック関数 (到達時に実行される関数)
}) ;

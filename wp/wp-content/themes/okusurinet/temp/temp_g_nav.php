<?php
/** The main template file. グローバルナビ */
?>

<div class="menu-bar">
	<div class="h-nav clearfix">
		<h1>お薬を仕入れる・安くする・不良在庫を減らす仕組みができました。</h1>
		<a class="hd-logo" href="<?php echo esc_url( home_url()); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/images/common/img_hd_logo.png" alt="" />
		</a>
		<div class="cta">
			<?php if (is_user_logged_in()) : // ログイン中 ?>
				<a href="<?php echo esc_url( home_url()); ?>/mypage/#ask-form" class="bt-box bt-contact"><span class="icon-ic icon-ic-mail"></span><span>不動在庫移動</span></a>
				<a href="./?a=logout" class="bt-box bt-logout"><span class="icon-ic icon-ic-enter"></span><span>ログアウト</span></a>
			<?php else : // ログインしていないとき ?>
				<a href="<?php echo esc_url( home_url()); ?>/#c-form" class="bt-box bt-contact"><span class="icon-ic icon-ic-mail"></span><span>お問合わせ</span></a>
				<a href="<?php echo esc_url( home_url()); ?>/mypage" class="bt-box bt-login"><span class="icon-ic icon-ic-enter"></span><span>加盟薬局ログイン</span></a>
			<?php endif; ?>
			
			<div class="tel">
				<span class="icon-ic-tel"></span><span class="tel-no">03-5937-1628</span>
				<p class="biz-hour">受付：平日 9:00〜17:30（土日祝日除く）</p>	
			</div>
		</div>
	</div>
</div><!-- // .menu-bar END -->
<div class="spacer-nav"></div>
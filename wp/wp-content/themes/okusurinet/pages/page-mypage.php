<?php
/** The main template file. 加盟薬局ログインページ */

//確認ページで「戻る」を押した時に、入力した内容をそのまま表示させる記述です。	
session_start();
$m_1_01='';
$m_1_03='';
$m_1_04='';
$m_1_05='';

if( isset($_SESSION['m_1_01'])){ $m_1_01 = $_SESSION['m_1_01']; }
$_POST['m_1_02_return'] = $_SESSION['m_1_02_return'];
if( isset($_SESSION['m_1_03'])){ $m_1_03 = $_SESSION['m_1_03']; }
if( isset($_SESSION['m_1_04'])){ $m_1_04 = $_SESSION['m_1_04']; }
if( isset($_SESSION['m_1_05'])){ $m_1_05 = $_SESSION['m_1_05']; }
$_POST['m_1_06_return'] = $_SESSION['m_1_06_return'];
if( isset($_SESSION['m_1_07'])){ $m_1_07 = $_SESSION['m_1_07']; }
if( isset($_SESSION['m_1_08'])){ $m_1_08 = $_SESSION['m_1_08']; }
if( isset($_SESSION['m_1_09'])){ $m_1_09 = $_SESSION['m_1_09']; }
if( isset($_SESSION['m_1_10'])){ $m_1_10 = $_SESSION['m_1_10']; }
if( isset($_SESSION['m_1_11'])){ $m_1_11 = $_SESSION['m_1_11']; }
if( isset($_SESSION['m_1_12'])){ $m_1_12 = $_SESSION['m_1_12']; }
if( isset($_SESSION['m_1_13'])){ $m_1_13 = $_SESSION['m_1_13']; }

////// リスト //////
$dev_list = array(
''=>'--', '内服'=>'内服', '外用'=>'外用',
);
$str_list = array(
''=>'--', '先発品'=>'先発品', '後発品'=>'後発品',
);


?>

<div id="contents">
	<?php if (is_user_logged_in()) : // ログイン中 ?>
	
	<div class="bc_F6F8FA ">
		<section class="mem-intro">
			<h2><?php global $current_user; echo $current_user->nickname; // 加盟薬局名 表示 ?></h2>
			<div class="mem-nav">
				<ul>
					<li>
						<a class="bt-box guide" href=""><span class="icon-ic icon-ic-stack"></span><span>ご利用ガイド</span></a>
					</li>
					<li>
						<a class="bt-box guide" href=""><span class="icon-ic icon-ic-file"></span><span>不動品一覧</span></a>
					</li>
					<li>
						<a class="bt-box guide" href=""><span class="icon-ic icon-ic-quest"></span><span>Q & A</span></a>
					</li>
				</ul>
			</div>
		</section>
		
		<section class="mem-topics">
			<h2><span class="icon-ic-hexa"></span><span class="jpn">最新情報</span><span class="eng">INFORMATION</span></h2>
			<div class="u-l"></div>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				$args = array(
				     'post_type' => 'post', // 投稿タイプを指定
				     'posts_per_page' => 5, // 表示するページ数
				     'paged' => $paged, // 表示するページ数
			); ?>
			<?php $wp_query = new WP_Query( $args ); ?><!-- クエリの指定 -->
				<ul class="topics-list">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
						<a class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php wp_reset_postdata(); ?>
		</section>
		<!-- // .mem-topics end -->
		 
	</div>
	<!-- // .bc_F6F8FA end -->

	<div class="bc_E1E5D6 ">
		<a id="ask-form" name="ask-form"></a>
		<section class="ask-form">
			<h2>不動在庫移動申込フォーム</h2>
			<form class="apply-form" action="<?php echo esc_url( home_url('/')); ?>confirm" method="POST">
				
				<table class="unit-tbl">
					<caption>薬品１</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="j_code_1" id="j_code_1" value="<?php echo $m_1_01; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="slc_dev_1" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if($value == $_POST['m_1_02_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="m_pac_1" id="m_pac_1" placeholder="包装形態（例:PTP）" value="<?php echo $m_1_03; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="b_quant_1" id="b_quant_1" placeholder="売却数量 (総数)" value="<?php echo $m_1_04; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_company_1" id="m_company_1" value="<?php echo $m_1_05; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="slc_str_1" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if($value == $_POST['m_1_06_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="p_quant_1" id="p_quant_1" placeholder="包装数量（例:15錠×6）" value="<?php echo $m_1_07; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_price_1" id="m_price_1" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo $m_1_08; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_name_1" id="m_name_1" value="<?php echo $m_1_09; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_norm_1" id="m_norm_1" placeholder="規格（例:20mg）" value="<?php echo $m_1_10; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_1" id="state_1" placeholder="保存状態（開封済み）" value="<?php echo $m_1_11; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_1" id="experiod_1" placeholder="使用期限（2020/12）" value="<?php echo $m_1_12; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="m_note_1" id="m_note_1" placeholder="備考内容" value="<?php echo $m_1_13; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 １ -->
				
				<table class="unit-tbl">
					<caption>薬品２</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="j_code_2" id="j_code_2" value="<?php echo $m_2_01; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="slc_dev_2" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if($value == $_POST['m_2_02_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="m_pac_2" id="m_pac_2" placeholder="包装形態（例:PTP）" value="<?php echo $m_2_03; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="b_quant_2" id="b_quant_2" placeholder="売却数量 (総数)" value="<?php echo $m_2_04; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_company_2" id="m_company_2" value="<?php echo $m_2_05; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="slc_str_2" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if($value == $_POST['m_2_06_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="p_quant_2" id="p_quant_2" placeholder="包装数量（例:15錠×6）" value="<?php echo $m_2_07; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_price_2" id="m_price_2" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo $m_2_08; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_name_2" id="m_name_2" value="<?php echo $m_2_09; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_norm_2" id="m_norm_2" placeholder="規格（例:20mg）" value="<?php echo $m_2_20; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_2" id="state_2" placeholder="保存状態（開封済み）" value="<?php echo $m_2_21; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_2" id="experiod_2" placeholder="使用期限（2020/12）" value="<?php echo $m_2_22; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="m_note_2" id="m_note_2" placeholder="備考内容" value="<?php echo $m_2_23; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ２ -->

				<table class="unit-tbl">
					<caption>薬品３</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="j_code_3" id="j_code_3" value="<?php echo $m_3_01; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="slc_dev_3" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if($value == $_POST['m_3_02_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="m_pac_3" id="m_pac_3" placeholder="包装形態（例:PTP）" value="<?php echo $m_3_03; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="b_quant_3" id="b_quant_3" placeholder="売却数量 (総数)" value="<?php echo $m_3_04; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_company_3" id="m_company_3" value="<?php echo $m_3_05; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="slc_str_3" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if($value == $_POST['m_3_06_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="p_quant_3" id="p_quant_3" placeholder="包装数量（例:15錠×6）" value="<?php echo $m_3_07; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_price_3" id="m_price_3" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo $m_3_08; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_name_3" id="m_name_3" value="<?php echo $m_3_09; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_norm_3" id="m_norm_3" placeholder="規格（例:20mg）" value="<?php echo $m_3_30; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_3" id="state_3" placeholder="保存状態（開封済み）" value="<?php echo $m_3_31; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_3" id="experiod_3" placeholder="使用期限（2020/12）" value="<?php echo $m_3_32; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="m_note_3" id="m_note_3" placeholder="備考内容" value="<?php echo $m_3_33; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ３ -->
				
				<table class="unit-tbl">
					<caption>薬品４</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="j_code_4" id="j_code_4" value="<?php echo $m_4_01; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="slc_dev_4" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if($value == $_POST['m_4_02_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="m_pac_4" id="m_pac_4" placeholder="包装形態（例:PTP）" value="<?php echo $m_4_03; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="b_quant_4" id="b_quant_4" placeholder="売却数量 (総数)" value="<?php echo $m_4_04; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_company_4" id="m_company_4" value="<?php echo $m_4_05; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="slc_str_4" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if($value == $_POST['m_4_06_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="p_quant_4" id="p_quant_4" placeholder="包装数量（例:15錠×6）" value="<?php echo $m_4_07; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_price_4" id="m_price_4" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo $m_4_08; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_name_4" id="m_name_4" value="<?php echo $m_4_09; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_norm_4" id="m_norm_4" placeholder="規格（例:20mg）" value="<?php echo $m_4_40; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_4" id="state_4" placeholder="保存状態（開封済み）" value="<?php echo $m_4_41; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_4" id="experiod_4" placeholder="使用期限（2020/12）" value="<?php echo $m_4_42; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="m_note_4" id="m_note_4" placeholder="備考内容" value="<?php echo $m_4_43; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ４ -->
				
				<table class="unit-tbl">
					<caption>薬品５</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="j_code_5" id="j_code_5" value="<?php echo $m_5_01; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="slc_dev_5" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if($value == $_POST['m_5_02_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="m_pac_5" id="m_pac_5" placeholder="包装形態（例:PTP）" value="<?php echo $m_5_03; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="b_quant_5" id="b_quant_5" placeholder="売却数量 (総数)" value="<?php echo $m_5_04; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_company_5" id="m_company_5" value="<?php echo $m_5_05; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="slc_str_5" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if($value == $_POST['m_5_06_return']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="p_quant_5" id="p_quant_5" placeholder="包装数量（例:15錠×6）" value="<?php echo $m_5_07; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_price_5" id="m_price_5" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo $m_5_08; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="m_name_5" id="m_name_5" value="<?php echo $m_5_09; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="m_norm_5" id="m_norm_5" placeholder="規格（例:20mg）" value="<?php echo $m_5_50; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_5" id="state_5" placeholder="保存状態（開封済み）" value="<?php echo $m_5_51; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_5" id="experiod_5" placeholder="使用期限（2020/12）" value="<?php echo $m_5_52; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="m_note_5" id="m_note_5" placeholder="備考内容" value="<?php echo $m_5_53; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ５ -->
				
				<div class="submit-area clearfix">
					<div class="submit">
						<input id="submit" type="submit" value="確認画面へ進む" class="bt-submit" />
					</div>
				</div><!— // .submit-ara END —>
			</form>

		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->
	
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/customSelect/jquery.customSelect.min.js"></script>
<script>
jQuery(document).ready(function(){
	$('.ssBox').customSelect(); // セレクトボックス デザイン
});
</script>

	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>

<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
	<?php endif; ?>
</div><!-- // #contents END -->
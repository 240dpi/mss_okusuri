<?php
/** The main template file. 不動在庫移動申込 確認ページ */

//$_POST['name'] →$nameへ、$name= 値 といった具合で挿入されます
foreach($_POST as $k=>$v){
  $$k=$v;
}
//値をセッションに入れる

session_start();

$_SESSION['m_1_01']=$j_code_1;
$_SESSION['m_1_02_return']=$slc_dev_1;
$_SESSION['m_1_03']=$m_pac_1;
$_SESSION['m_1_04']=$b_quant_1;
$_SESSION['m_1_05']=$m_company_1;
$_SESSION['m_1_06_return']=$slc_str_1;
$_SESSION['m_1_07']=$p_quant_1;
$_SESSION['m_1_08']=$m_price_1;
$_SESSION['m_1_09']=$m_name_1;
$_SESSION['m_1_10']=$m_norm_1;
$_SESSION['m_1_11']=$state_1;
$_SESSION['m_1_12']=$experiod_1;
$_SESSION['m_1_13']=$m_note_1;

?>

<div id="contents">
	<?php if (is_user_logged_in()) : // ログイン中 ?>

	<div class="bc_E1E5D6 ">
	
		<a id="ask-form" name="ask-form"></a>
		<section class="ask-form">
			<h2>申込内容確認</h2>
			<div class="conf-form">
				<table class="unit-tbl tbl-head">
					<tr>
						<td class="td-1">
							<span>JANコード</span>
						</td>
						<td class="td-2">
							<span>区分</span>
						</td>
						<td class="td-3">
							<span>包装形態</span>
						</td>
						<td class="td-4">
							<span>売却数量（総数）</span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>薬価小計</span>
							</div>
							<div class="s-price">
								<span>取引金額</span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span>製造会社</span>
						</td>
						<td>
							<span>先発</span>
						</td>
						<td>
							<span>包装数量</span>
						</td>
						<td>
							<span>薬価</span>
						</td>
					</tr>
					<tr>
						<td>
							<span>医薬品名</span>
						</td>
						<td>
							<span>規格</span>
						</td>
						<td>
							<span>保存状態</span>
						</td>
						<td>
							<span>使用期限</span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<p>備考内容</p>
						</td>
					</tr>
				</table><!-- テーブルヘッダー -->
					
				<table class="unit-tbl">
					<tr>
						<td class="td-1">
							<span><?php echo $j_code_1; ?></span>
						</td>
						<td class="td-2">
							<span><?php echo $slc_dev_1; ?></span>
						</td>
						<td class="td-3">
							<span><?php echo $m_pac_1; ?></span>
						</td>
						<td class="td-4">
							<span><?php echo $b_quant_1; ?></span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>¥ <?php echo number_format(floor($b_quant_1*$m_price_1)); ?></span>
							</div>
							<div class="s-price">
								<span>¥ <?php echo  number_format(floor($b_quant_1*$m_price_1*0.5)); ?></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_company_1; ?></span>
						</td>
						<td>
							<span><?php echo $slc_str_1; ?></span>
						</td>
						<td>
							<span><?php echo $p_quant_1; ?></span>
						</td>
						<td>
							<span><?php echo $m_price_1; ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_name_1; ?></span>
						</td>
						<td>
							<span><?php echo $m_norm_1; ?></span>
						</td>
						<td>
							<span><?php echo $state_1; ?></span>
						</td>
						<td>
							<span><?php echo $experiod_1; ?></span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span><?php echo $m_note_1; ?></span>
						</td>
					</tr>
				</table><!-- 薬品 １ -->

				<table class="unit-tbl">
					<tr>
						<td class="td-1">
							<span><?php echo $j_code_2; ?></span>
						</td>
						<td class="td-2">
							<span><?php echo $slc_dev_2; ?></span>
						</td>
						<td class="td-3">
							<span><?php echo $m_pac_2; ?></span>
						</td>
						<td class="td-4">
							<span><?php echo $b_quant_2; ?></span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>¥ <?php echo number_format(floor($b_quant_2*$m_price_2)); ?></span>
							</div>
							<div class="s-price">
								<span>¥ <?php echo  number_format(floor($b_quant_2*$m_price_2*0.5)); ?></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_company_2; ?></span>
						</td>
						<td>
							<span><?php echo $slc_str_2; ?></span>
						</td>
						<td>
							<span><?php echo $p_quant_2; ?></span>
						</td>
						<td>
							<span><?php echo $m_price_2; ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_name_2; ?></span>
						</td>
						<td>
							<span><?php echo $m_norm_2; ?></span>
						</td>
						<td>
							<span><?php echo $state_2; ?></span>
						</td>
						<td>
							<span><?php echo $experiod_2; ?></span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span><?php echo $m_note_2; ?></span>
						</td>
					</tr>
				</table><!-- 薬品 ２ -->
				
				<table class="unit-tbl">
					<tr>
						<td class="td-1">
							<span><?php echo $j_code_3; ?></span>
						</td>
						<td class="td-2">
							<span><?php echo $slc_dev_3; ?></span>
						</td>
						<td class="td-3">
							<span><?php echo $m_pac_3; ?></span>
						</td>
						<td class="td-4">
							<span><?php echo $b_quant_3; ?></span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>¥ <?php echo number_format(floor($b_quant_3*$m_price_3)); ?></span>
							</div>
							<div class="s-price">
								<span>¥ <?php echo  number_format(floor($b_quant_3*$m_price_3*0.5)); ?></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_company_3; ?></span>
						</td>
						<td>
							<span><?php echo $slc_str_3; ?></span>
						</td>
						<td>
							<span><?php echo $p_quant_3; ?></span>
						</td>
						<td>
							<span><?php echo $m_price_3; ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_name_3; ?></span>
						</td>
						<td>
							<span><?php echo $m_norm_3; ?></span>
						</td>
						<td>
							<span><?php echo $state_3; ?></span>
						</td>
						<td>
							<span><?php echo $experiod_3; ?></span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span><?php echo $m_note_3; ?></span>
						</td>
					</tr>
				</table><!-- 薬品 ３ -->
				
								<table class="unit-tbl">
					<tr>
						<td class="td-1">
							<span><?php echo $j_code_4; ?></span>
						</td>
						<td class="td-2">
							<span><?php echo $slc_dev_4; ?></span>
						</td>
						<td class="td-3">
							<span><?php echo $m_pac_4; ?></span>
						</td>
						<td class="td-4">
							<span><?php echo $b_quant_4; ?></span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>¥ <?php echo number_format(floor($b_quant_4*$m_price_4)); ?></span>
							</div>
							<div class="s-price">
								<span>¥ <?php echo  number_format(floor($b_quant_4*$m_price_4*0.5)); ?></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_company_4; ?></span>
						</td>
						<td>
							<span><?php echo $slc_str_4; ?></span>
						</td>
						<td>
							<span><?php echo $p_quant_4; ?></span>
						</td>
						<td>
							<span><?php echo $m_price_4; ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_name_4; ?></span>
						</td>
						<td>
							<span><?php echo $m_norm_4; ?></span>
						</td>
						<td>
							<span><?php echo $state_4; ?></span>
						</td>
						<td>
							<span><?php echo $experiod_4; ?></span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span><?php echo $m_note_4; ?></span>
						</td>
					</tr>
				</table><!-- 薬品 4 -->
				
				<table class="unit-tbl">
					<tr>
						<td class="td-1">
							<span><?php echo $j_code_5; ?></span>
						</td>
						<td class="td-2">
							<span><?php echo $slc_dev_5; ?></span>
						</td>
						<td class="td-3">
							<span><?php echo $m_pac_5; ?></span>
						</td>
						<td class="td-4">
							<span><?php echo $b_quant_5; ?></span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>¥ <?php echo number_format(floor($b_quant_5*$m_price_5)); ?></span>
							</div>
							<div class="s-price">
								<span>¥ <?php echo  number_format(floor($b_quant_5*$m_price_5*0.5)); ?></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_company_5; ?></span>
						</td>
						<td>
							<span><?php echo $slc_str_5; ?></span>
						</td>
						<td>
							<span><?php echo $p_quant_5; ?></span>
						</td>
						<td>
							<span><?php echo $m_price_5; ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $m_name_5; ?></span>
						</td>
						<td>
							<span><?php echo $m_norm_5; ?></span>
						</td>
						<td>
							<span><?php echo $state_5; ?></span>
						</td>
						<td>
							<span><?php echo $experiod_5; ?></span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span><?php echo $m_note_5; ?></span>
						</td>
					</tr>
				</table><!-- 薬品 5 -->
				
				<div class="total">
					<div class="total-d">
						<span class="ttl">薬価合計</span>
						<span class="price">¥ <?php echo number_format(floor($b_quant_1*$m_price_1)); ?></span>
					</div>
					<div class="total-s">
						<span class="ttl">取引金額合計</span>
						<span class="price">¥ <?php echo  number_format(floor($b_quant_1*$m_price_1*0.5)); ?></span>
					</div>
				</div>

				<div class="submit-area clearfix">
					<div class="submit">
						<a class="bt-back" href="<?php echo esc_url( home_url('/')); ?>mypage#ask-form">編集へ戻る</a>
						<a class="bt-send" href="">この内容で送信</a>
					</div>
				</div><!— // .submit-ara END —>
			</div><!-- //.conf-form  -->
		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->

	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>

<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
	<?php endif; ?>
</div><!-- // #contents END -->